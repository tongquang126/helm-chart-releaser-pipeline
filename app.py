
import streamlit as st
import http.client

#The function to get Device Status 
def device_status(apikey,deviceid):
  request_url = "/v1/nebula/" + deviceid + "/online-status"
  conn = http.client.HTTPSConnection("gamma-oapi.nebula.zyxel.com")
  payload = ''
  headers = {
    'X-ZyxelNebula-API-Key': apikey
  }
  conn.request("GET", request_url, payload, headers)
  res = conn.getresponse()
  data = res.read()
  data = data.decode("utf-8")
  if "ONLINE" in data:
    status = "Your device is ONLINE"
  elif "OFFLINE" in data:
    status = "Your device is OFFLINE"
  else:
    status = "Operation Failed! Please check your inputs"
  return status

#The main function 
if __name__ == "__main__":
   st.title("Device Status Checker")
   apikey = st.text_input("API Key", "Input API Key (e.g,.AUxQtMvI97KLcFhBP9)")
   deviceid = st.text_input("Device ID", "Input Device ID (e.g.,616d15e813ef9c6633428380)")
   result = st.button("Check")
   if result:
      #Display device status
      status = device_status(apikey,deviceid)
      st.write(f'{status}')
