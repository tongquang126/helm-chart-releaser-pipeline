# About the project

## The project to create a simple web app with Python and streamlit
* I selft-make a web application helps Zyxel customers can montinor their device status on our cloud   
* Web application uses RESTful API to perform request to Nebula, and display the result 
 
  ![image](/uploads/796b290a53ead3628fe3962a0dc716b8/image.png)

## Prerequisite
* You must have GKE cluster, and install GitLab agent to safety connect to the GitLab (Refer GitLab CICD with a Kubernetes cluster.pdf)

  ![image](/uploads/c3958e68e744c1c1b02f601d422168e7/image.png)

## The CICD Pipeline 
* Pipeline flow: GitLab Repo ---[trigger]---> GitLab CI ---[build and push docker image]---> GitLab Container Registry ---[package and upload Helm chart] --->AWS S3 ---[deploy]--->GKE cluster

  ![image](/uploads/1241de232e59efd4960e38073bf0b4be/image.png)

* Pipeline stages
  - Build and push docker image: Build docker image from Dockerfile,then push to GitLab Container Registry
  - Create Helm chart: package the helm chart 
  - Upload Helm chart to AWS S3: upload the helm chart to S3 which works as helm chart repository
  - Deploy app using Helm: Deploy the web application on GKE cluster. Note that, for production environment, you should deploy the web application on staging environment beforehand. 
  - Verify application deployment: The web application be accessible by external load balancer service. This stage will get the loadBalancerIP, and use GET method to check the web application health.
  - Clean up: Clear up the cluster resources

  ![image](/uploads/fbb355ab68ef8f132b32792c59e1057d/image.png)
  ![image](/uploads/70c14246b49d2945312d5eca0536966f/image.png)

## Monitoring and Notification

* To monitor GKE cluster using Prometeus & Grafana, you can deploy using Google Cloud Marketplace

* To monitor the events for the pipelines, go to Project > Settings > Integrations, then add Slack notification 

  ![image](/uploads/22b9f8c70231f2dafc453ee84d928ff8/image.png)
  
* GitLab will send the notification to Slack when git commit, push,... or pipeline status changes

  ![image](/uploads/d26e14ec49e0cd1241199d81f7ea3397/image.png)  


